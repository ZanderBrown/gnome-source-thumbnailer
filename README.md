# Source Thumbnailer

Thumbnails for your code

Simple little pictures of the first few lines of content with a little badge

Nothing fancy, just install and enjoy

**NOTE**: Must be installed system wide, recent versions of GNOME only allow
thumbnailers installed under /usr for security reasons
